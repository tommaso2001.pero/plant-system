import unittest

import plant_system


class Plant_systemTestCase(unittest.TestCase):

    def setUp(self):
        self.app = plant_system.app.test_client()

    def test_index(self):
        rv = self.app.get('/')
        self.assertIn('Welcome to Plant Controll System', rv.data.decode())


if __name__ == '__main__':
    unittest.main()
