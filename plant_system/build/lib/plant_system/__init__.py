import os
from flask import Flask, request, send_from_directory
from flask_bower import Bower

app = Flask(__name__)
app.config.from_object('plant_system.default_settings')
app.config.from_envvar('PLANT_SYSTEM_SETTINGS')
app.config['TEMPLATES_AUTO_RELOAD'] = True
if not app.debug:
    import logging
    from logging.handlers import TimedRotatingFileHandler
    # https://docs.python.org/3.6/library/logging.handlers.html#timedrotatingfilehandler
    file_handler = TimedRotatingFileHandler(os.path.join(
        app.config['LOG_DIR'], 'plant_system.log'), 'midnight')
    file_handler.setLevel(logging.WARNING)
    file_handler.setFormatter(logging.Formatter(
        '<%(asctime)s> <%(levelname)s> %(message)s'))
    app.logger.addHandler(file_handler)


# @app.route('/bower/<path:path>')
# def send_js(path):
#     return send_from_directory('bower', path)

Bower(app)
import plant_system.views
import plant_system.api
