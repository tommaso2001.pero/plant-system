from flask import Flask, request, Response
from flask_restful import Resource, Api
from . import app
from subprocess import call
import random
import json
import os
import math
api = Api(app)


sensors_data = {}


class sens_data(Resource):
    def get(self):
        sensors_data['temp'] = int(open('/sys/class/i2c-dev/i2c-1/device/1-0040/temp1_input').read())
        sensors_data['water'] = int(open('/sys/bus/iio/devices/iio:device1/in_voltage0_raw').read())
        return json.dumps(sensors_data)

api.add_resource(sens_data, '/get/sens_data')


@app.route('/gpio/d<int:port>/<int:state>')
def change_state(port, state):
	if (state==1 or state==0 and math.isnan(port)):
		os.system('echo ' +  str(state) + ' > /sys/class/gpio/gpio104/value')
	return json.dumps({'success':True}), 200, {'ContentType':'application/json'} 
