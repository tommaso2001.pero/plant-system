from flask import Flask, request, Response
from flask_restful import Resource, Api
from . import app
from subprocess import call
import random
import json
import os

api = Api(app)


sensors_data = {}


class sens_data(Resource):
    def get(self):
        sensors_data['temp'] = int(open('/sys/class/i2c-dev/i2c-1/device/1-0040/temp1_input').read())
        sensors_data['water'] = int(open('/sys/bus/iio/devices/iio:device0/in_voltage0_raw').read())
        ir_data = int(open('/sys/class/i2c-dev/i2c-1/device/1-0029/iio:device2/in_intensity_ir_raw').read())
        total_light_data = int(open('/sys/class/i2c-dev/i2c-1/device/1-0029/iio:device2/in_intensity_both_raw').read())
        sensors_data['light'] = total_light_data - ir_data
        return json.dumps(sensors_data)

api.add_resource(sens_data, '/get/sens_data')


@app.route('/gpio/d2/<int:state>')
def change_state(state):
	if (state==1 or state==0):
		os.system('echo ' +  str(state) + ' > /sys/class/gpio/gpio104/value')
	return json.dumps({'success':True}), 200, {'ContentType':'application/json'} 
