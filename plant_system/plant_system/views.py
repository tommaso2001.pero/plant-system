from flask import render_template
from plant_system import app,basic_auth


@app.route('/')
@basic_auth.required
def index():
    app.logger.warning('sample message')
    return render_template('index.html')


