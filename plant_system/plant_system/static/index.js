window.onload = function onLoad() {

	var water_bar = new ldBar("#water_bar");
  var light_bar = new ldBar("#light_bar");
  var temp_bar = new ldBar("#temp_bar");
	setInterval(function(){	
    		$.get( "get/sens_data", function( data ) {
              data = JSON.parse(data);
            	temp_bar.set(data.temp/500);
              $('#temp_value').html("" + Math.round(data.temp/1000) + " &deg;C");
              water_bar.set(data.water/35);
              $('#water_value').html("" + Math.round(data.water/35) + "%");    
              light_bar.set(data.light/500);
              $('#light_value').html("" + Math.round(data.light/10) + " Lumen");
            	});
        }, 3000); 
 // Number from 0.0 to 1.0
};
